import React from 'react';
import {
    Image,
} from 'react-native';

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import Home from '../screens/Home';
import Album from '../screens/Album';
import Tag from '../screens/Tag';
import Setting from '../screens/Setting';
import { COLORS } from '../constants/theme';
import { ICONS } from '../constants/icons';
import { createStackNavigator } from '@react-navigation/stack';
import VideoAlbum from '../screens/VideoAlbum';
import { DATA } from '../constants/data';
import TagVideo from '../screens/TagVideo';


const Tab = createBottomTabNavigator();
const AlbumStack = createStackNavigator();
const TagStack = createStackNavigator();

const AlbumStackScreen = () => {
    return (
        <AlbumStack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <AlbumStack.Screen name="AlbumScreen" component={Album} />
            <AlbumStack.Screen
                name="VideoAlbum"
                component={VideoAlbum}
            />
        </AlbumStack.Navigator>
    );
}

const TagStackScreen = () => {
    return (
        <AlbumStack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <AlbumStack.Screen name="TagScreen" component={Tag} />
            <AlbumStack.Screen
                name="TagVideo"
                component={TagVideo}
            />
        </AlbumStack.Navigator>
    );
}


const TabBar = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarShowLabel: true,
                tabBarHideOnKeyboard: true,
                tabBarStyle: {
                    height: 40
                }
            }}
        >
            <Tab.Screen
                name={DATA.homePage}
                component={Home}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={ICONS.gallery}
                            resizeMode="contain"
                            style={{
                                width: 20,
                                height: 20,
                                tintColor: focused ? COLORS.blue : COLORS.gray
                            }}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name={DATA.album}
                component={AlbumStackScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={ICONS.album}
                            resizeMode="contain"
                            style={{
                                width: 20,
                                height: 20,
                                tintColor: focused ? COLORS.blue : COLORS.gray
                            }}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name={DATA.tags}
                component={TagStackScreen}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={ICONS.tag}
                            resizeMode="contain"
                            style={{
                                width: 20,
                                height: 20,
                                tintColor: focused ? COLORS.blue : COLORS.gray
                            }}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name={DATA.setting}
                component={Setting}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={ICONS.setting}
                            resizeMode="contain"
                            style={{
                                width: 20,
                                height: 20,
                                tintColor: focused ? COLORS.blue : COLORS.gray
                            }}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

export default TabBar;
