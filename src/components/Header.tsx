import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { useState } from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import { ICONS } from '../constants/icons';
import { COLORS, SIZES } from '../constants/theme';


const Header = () => {

    const navigation = useNavigation<StackNavigationProp<any, any>>();

    const [isVisibleSearch, setVisibleSearch] = useState(false);

    return (
        <View style={styles.container}>
            {isVisibleSearch === false &&
                <View style={styles.left}>
                    <Text style={styles.title}> My video</Text>
                </View>
            }
            <View style={styles.right}>
                {isVisibleSearch === true &&
                    <View style={styles.searchContainer}>
                        <TouchableOpacity style={styles.iconBackButton} onPress={() => setVisibleSearch(false)}>
                            <Image
                                style={styles.iconBack}
                                source={ICONS.back} />
                        </TouchableOpacity>
                        <View style={styles.searchBox}>
                            <TextInput
                                style={styles.textSearch}
                                placeholder="Search"
                                keyboardType="default"
                                autoFocus={true}
                            />
                            <Image
                                style={styles.iconSearchInSearchBox}
                                source={ICONS.search} />
                            <Image
                                style={styles.iconMicrophone}
                                source={ICONS.microphone} />
                        </View>
                        <TouchableOpacity onPress={() => navigation.navigate('Home', { search: 'dataxxxxxxxxxxxxxxx' })}>
                            <Text style={styles.searchText}>Search</Text>
                        </TouchableOpacity>
                    </View>
                }
                {isVisibleSearch === false &&
                    <View style={styles.icons}>
                        <TouchableOpacity onPress={() => setVisibleSearch(true)}>
                            <Image
                                style={styles.icon}
                                source={ICONS.search}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image
                                style={styles.icon}
                                source={ICONS.camera}
                            />
                        </TouchableOpacity>
                    </View>
                }
            </View>
        </View>
    );
}

var styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: COLORS.gray,
        borderBottomWidth: 0.8,
        padding: 8,
        justifyContent: 'space-between'
    },
    left: {
        width: '30%'
    },
    title: {
        color: COLORS.blue,
        fontSize: SIZES.h4
    },
    right: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    searchText: {
        color: COLORS.blue
    },
    icons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.gray,
        marginLeft: 15
    },
    iconBackButton: {
        width: '8%'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue
    },
    searchBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 5,
        width: '75%',
        backgroundColor: COLORS.gray
    },
    textSearch: {
        position: 'relative',
        color: 'black',
        width: '100%',
        padding: 2,
        paddingLeft: 25,
        borderWidth: 0
    },
    iconSearchInSearchBox: {
        width: 17,
        height: 17,
        position: 'absolute',
        marginLeft: 5,
        tintColor: COLORS.black
        // flex: 1,
    },
    iconMicrophone: {
        width: 17,
        height: 17,
        position: 'absolute',
        // top: 0,
        right: 5,
    }
});

export default Header;
