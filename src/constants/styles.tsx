import { Dimensions, StyleSheet } from "react-native";
import { COLORS } from "./theme";

export const constantStyles = StyleSheet.create({
    container: {
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
    },
    videoList: {
        flexDirection: 'column',
        // justifyContent: 'space-around',
        // flexWrap: 'wrap',
    },
    album: {
        marginBottom: 5,
        width: Dimensions.get('window').width / 5,
        height: 90,
        position: 'relative'
    },
    albumImg: {
        width: 120,
        height: 120,
        borderRadius: 5,
    },

    alBumInfo: {
        // position: 'absolute',
        // bottom: 0,
        // left: 0,
    }

});