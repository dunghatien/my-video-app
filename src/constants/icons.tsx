const gallery = require("../../assets/icons/gallery.png");
const search = require("../../assets/icons/search.png");
const album = require("../../assets/icons/album.png");
const setting = require('../../assets/icons/setting.png');
const back = require('../../assets/icons/back.png');
const camera = require('../../assets/icons/camera.png');
const check = require('../../assets/icons/check.png');
const fingerprint = require('../../assets/icons/fingerprint.png');
const importData = require('../../assets/icons/import.png');
const exportData = require('../../assets/icons/export.png');
const dataCleaning = require('../../assets/icons/data-cleaning.png');
const help = require('../../assets/icons/help.png');
const wrench = require('../../assets/icons/wrench.png');
const play = require('../../assets/icons/play.png');
const share = require('../../assets/icons/share.png');
const heart = require('../../assets/icons/heart.png');
const pause = require('../../assets/icons/pause.png');
const mute = require('../../assets/icons/mute.png');
const volume = require('../../assets/icons/volume.png');
const trash = require('../../assets/icons/trash.png');
const microphone = require('../../assets/icons/microphone.png');
const add = require('../../assets/icons/add.png');
const threeDot = require('../../assets/icons/threeDot.png');
const folder = require('../../assets/icons/folder.png');
const playButton = require('../../assets/icons/play-button.png');
const record = require('../../assets/icons/record.png');
const tag = require('../../assets/icons/tag.png');
const videoCamera = require('../../assets/icons/video-camera.png');
const switchCamera = require('../../assets/icons/switch-camera.png');
const close = require('../../assets/icons/close.png');




export const ICONS = {
    gallery,
    search,
    album,
    setting,
    back,
    camera,
    check,
    fingerprint,
    importData,
    exportData,
    dataCleaning,
    help,
    wrench,
    play,
    share,
    heart,
    pause,
    mute,
    volume,
    trash,
    microphone,
    add,
    threeDot,
    folder,
    playButton,
    record,
    tag,
    videoCamera,
    switchCamera,
    close
}