export const DATA = {
    size: 50,
    homePage: 'Home',
    myVideo: 'My Video',
    album: 'Album',
    search: 'Search',
    setting: 'Setting',
    txtBack: 'Back',
    tags: 'Tags'
}