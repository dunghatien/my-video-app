export class TagModel {
    id: string = '';
    name: string = '';
    timestamp: number = 0;
    url: string = '';
    constructor(id: string, name: string, timestamp: number, url: string = '') {
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.url = url;
    }

};