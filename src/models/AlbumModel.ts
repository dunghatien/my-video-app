import { IMAGES } from "../constants/images";

export class AlbumModel {
    title: string = '';
    count: number = 0;
    firstImgUrl: string = '';
    constructor(title: string, count: number, firstImgUrl: string) {
        this.title = title;
        this.count = count;
        this.firstImgUrl = firstImgUrl === '' ? IMAGES.folder : firstImgUrl;
    }
}
