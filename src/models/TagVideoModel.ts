export class TagVideoModel {
    url: string = '';
    id: string = '';
    timestamp: number = 0;
    duration: number = 0;
    timestampVideoDownload: number = 0;
    idTag: number = 0;
    constructor(url: string, duration: number, timestampVideoDownload: number, idTag: number) {
        this.url = url;
        this.timestamp = new Date().getTime();
        this.duration = duration;
        this.timestampVideoDownload = timestampVideoDownload;
        this.idTag = idTag
    }
};