export class VideoModel {
    url: string = '';
    duration: string = '';
    timestamp: number = 0;
    durationNumber: string = '';
    constructor(url: string, duration: number, timestamp: number) {
        this.url = url;
        this.durationNumber = duration + '';
        const dateString = new Date(duration * 1000).toISOString().substr(11, 8);
        const hours = dateString.split(':')[0];
        const minutes = dateString.split(':')[1];
        const seconds = dateString.split(':')[2];
        if (parseInt(hours) < 1) {
            this.duration = parseInt(minutes) + ':' + parseInt(seconds);
        } else {
            this.duration = parseInt(hours) + ':' + parseInt(minutes) + ':' + parseInt(seconds);
        }
        this.timestamp = timestamp;
    }

}