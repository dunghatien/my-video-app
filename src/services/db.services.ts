import { enablePromise, openDatabase, ResultSet, SQLiteDatabase } from 'react-native-sqlite-storage';
import { TagModel } from '../models/TagModel';
import { TagVideoModel } from '../models/TagVideoModel';

const tableName = 'tagsVideo';
const tableTag = 'tags';

enablePromise(true);

export const getDBConnection = async () => {
    return openDatabase({ name: 'tags-video.db', location: 'default' });
};

export const createTable = async (db: SQLiteDatabase) => {
    const query = `CREATE TABLE IF NOT EXISTS ${tableName} (url TEXT NOT NULL, timestamp INTEGER NOT NULL, duration INTEGER, timestampVideoDownload INTEGER, idTag INTEGER);`;
    const query2 = `CREATE TABLE IF NOT EXISTS ${tableTag} (name TEXT NOT NULL, timestamp INTEGER NOT NULL);`;

    await db.executeSql(query);
    await db.executeSql(query2);

};

export const getVideoByTagId = async (db: SQLiteDatabase, tagId: number): Promise<TagVideoModel[]> => {
    try {
        const videoTas: TagVideoModel[] = [];
        const results = await db.executeSql(`SELECT rowid as id,url, timestamp, duration, timestampVideoDownload, idTag FROM ${tableName} where idTag= ${tagId} order by timestamp desc`);
        results.forEach(result => {
            for (let index = 0; index < result.rows.length; index++) {
                videoTas.push(result.rows.item(index))
            }
        });
        return videoTas;
    } catch (error) {
        console.error('error ' + JSON.stringify(error));
        throw Error('Failed to get videoTas !!!');
    }
};

export const getTags = async (db: SQLiteDatabase, whereStatement?: string) => {
    try {
        const videoTags: TagModel[] = [];
        const results: ResultSet[] = await db.executeSql(`SELECT rowid as id, name, timestamp  FROM ${tableTag} ${whereStatement} order by timestamp desc`);
        results.forEach(result => {
            for (let index = 0; index < result.rows.length; index++) {
                videoTags.push(result.rows.item(index))
            }
        });
        return videoTags;
    } catch (error) {
        console.error('error ' + JSON.stringify(error));
        throw Error('Failed to get videoTags !!!');
    }
};


export const getImageTags = async (db: SQLiteDatabase, idTag: number) => {
    try {
        const imageTas: string[] = [];
        const results: ResultSet[] = await db.executeSql(`SELECT url  FROM ${tableName} where idTag=${idTag} order by timestamp desc limit ${1}`);
        results.forEach(result => {
            for (let index = 0; index < result.rows.length; index++) {
                imageTas.push(result.rows.item(index).url)
            }
        });
        return imageTas[0];
    } catch (error) {
        console.error('error ' + JSON.stringify(error));
        throw Error('Failed to get videoTags !!!');
    }
};



export const getVideoTagsBySearchValue = async (db: SQLiteDatabase, txtSearch: string): Promise<TagVideoModel[]> => {
    try {
        const videoTas: TagVideoModel[] = [];
        const results = await db.executeSql(`SELECT rowid as id,url, tag, timestamp, duration, timestampVideoDownload FROM ${tableName} where tag like '%${txtSearch}%' order by timestamp desc`);
        results.forEach(result => {
            for (let index = 0; index < result.rows.length; index++) {
                videoTas.push(result.rows.item(index))
            }
        });
        return videoTas;
    } catch (error) {
        console.error('error ' + JSON.stringify(error));
        throw Error('Failed to get videoTas !!!');
    }
};


export const saveTagVideo = async (db: SQLiteDatabase, tagVideo: TagVideoModel) => {
    console.log("🚀 ~ file: db.services.ts ~ line 92 ~ saveTagVideo ~ tagVideo", tagVideo)
    const insertQuery = `INSERT OR REPLACE INTO ${tableName}(url, timestamp, duration, timestampVideoDownload, idTag) values ('${tagVideo.url}', ${new Date().getTime()}, ${tagVideo.duration}, ${tagVideo.timestampVideoDownload}, ${tagVideo.idTag})`;
    return db.executeSql(insertQuery);
};



export const saveTag = async (db: SQLiteDatabase, tag: string): Promise<ResultSet[]> => {
    const insertQuery = `INSERT OR REPLACE INTO ${tableTag}(name, timestamp) values ('${tag}',${new Date().getTime()})`;
    return db.executeSql(insertQuery);
};

export const deleteTagVideo = async (db: SQLiteDatabase, id: number) => {
    const deleteQuery = `DELETE from ${tableTag} where rowid = ${id}`;
    await db.executeSql(deleteQuery);
};

export const deleteTagVideoByUrlVideo = async (db: SQLiteDatabase, url: string) => {
    const deleteQuery = `DELETE from ${tableName} where url = '${url}'`;
    await db.executeSql(deleteQuery);
};

export const deleteTable = async (db: SQLiteDatabase) => {
    const query = `drop table ${tableName}`;
    const query2 = `drop table ${tableTag}`;

    await db.executeSql(query);
    await db.executeSql(query2);

};