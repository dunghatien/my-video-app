import { StackNavigationProp } from '@react-navigation/stack';
import React, { useCallback, useEffect, useState } from 'react';
import {
    Dimensions,
    Image,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    View,
    SafeAreaView,
    TextInput,
    PermissionsAndroid
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import CameraRoll from '@react-native-community/cameraroll';
import { VideoModel } from '../models/VideoModel';
import { ICONS } from '../constants/icons';
import { COLORS, SIZES } from '../constants/theme';
import { DATA } from '../constants/data';
import { useCamera } from 'react-native-camera-hooks';
import { createTable, getDBConnection, getVideoTagsBySearchValue } from '../services/db.services';
import { TagVideoModel } from '../models/TagVideoModel';

const Home = () => {

    const [refreshing, setRefreshing] = useState(false);
    const [findItem, setFindItem] = useState(DATA.size);
    useEffect(() => {
        async function requestCameraPermission() {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                    {
                        title: "Media Permission",
                        message:"Access to storage",
                        buttonNegative: "Cancel",
                        buttonPositive: "OK"
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("You can use the READ_EXTERNAL_STORAGE");
                    getAlbums();
                } else {
                    console.log("READ_EXTERNAL_STORAGE permission denied");
                }
            } catch (err) {
                console.warn(err);
            }
        }

        requestCameraPermission();

    }, [findItem]);
    const onRefresh = useCallback(() => {
        setFindItem(DATA.size);
        setRefreshing(true);
        getAlbums();
        setRefreshing(false);
    }, []);
    const [totalVideo, setTotalVideo] = useState(0);

    const navigation = useNavigation<StackNavigationProp<any, any>>();
    const [videoList, setVideoList] = useState<VideoModel[]>([]);
    const [videoSelected, setVideoSelected] = useState<number[]>([]);
    const [txtSearch, onChangeTextSearch] = useState("");


    const getAlbums = async () => {
        let videos: CameraRoll.PhotoIdentifier[] = [];
        let count: number = 0;
        const albums: CameraRoll.Album[] = await CameraRoll.getAlbums({ assetType: 'Videos' });
        for (let index = 0; index < albums.length; index++) {
            const title = albums[index].title;
            count += albums[index].count;
            var result = await getPhotosByAlbum(title);
            videos = videos.length === 0 ? result : (videos.concat(result));
        }
        setTotalVideo(count);
        videos.sort((a, b) => b.node.timestamp - a.node.timestamp);
        let list: VideoModel[] = [];
        videos.forEach((item: CameraRoll.PhotoIdentifier) => {
            const video: VideoModel = new VideoModel(item.node.image.uri, item.node.image.playableDuration !== null ? item.node.image.playableDuration : 0, item.node.timestamp);
            list.push(video);
        });
        setVideoList(list);
        const db = await getDBConnection();
        await createTable(db);
    }

    const getPhotosByAlbum = async (albumName: string) => {
        const result = await CameraRoll.getPhotos({
            first: findItem,
            groupName: albumName,
            assetType: 'Videos',
            include: ['fileSize', 'filename', 'imageSize', 'location', 'playableDuration']

        })
        const list: CameraRoll.PhotoIdentifier[] = [];
        result.edges.forEach((item) => {
            list.push(item);
        });
        return list;
    }

    const onSelectedItem = (url: string, index: number) => {
        videoSelected.push(index);
    }
    const [isVisibleSearch, setVisibleSearch] = useState(false);

    const handleLoadMore = () => {
        if (findItem < totalVideo) {
            setRefreshing(true);
            setFindItem(findItem + DATA.size);
            setRefreshing(false);
        }
    }

    const onHandleSearch = async () => {
        try {
            const db = await getDBConnection();
            let list: VideoModel[] = [];
            console.log("🚀 ~ file: Home.tsx ~ line 102 ~ onHandleSearch ~ txtSearch", txtSearch)
            const videoTags: TagVideoModel[] = await getVideoTagsBySearchValue(db, txtSearch);
            for (let index = 0; index < videoTags.length; index++) {
                let video = new VideoModel(videoTags[index].url, videoTags[index].duration, videoTags[index].timestampVideoDownload);
                list.push(video);
            }
            setVideoList(list);
        } catch (error) {
            setVideoList([]);
        }
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.containerHeader}>
                {isVisibleSearch === false &&
                    <View style={styles.left}>
                        <Text style={styles.title}>{DATA.myVideo}</Text>
                    </View>
                }
                <View style={styles.right}>
                    {isVisibleSearch === true &&
                        <View style={styles.searchContainer}>
                            <TouchableOpacity style={styles.iconBackButton} onPress={() => setVisibleSearch(false)}>
                                <Image
                                    style={styles.iconBack}
                                    source={ICONS.back} />
                            </TouchableOpacity>
                            <View style={styles.searchBox}>
                                <TextInput
                                    style={styles.textSearch}
                                    placeholder="Search"
                                    keyboardType="default"
                                    autoFocus={true}
                                    onChangeText={onChangeTextSearch}
                                    value={txtSearch}
                                />
                                <Image
                                    style={styles.iconSearchInSearchBox}
                                    source={ICONS.search} />
                                <TouchableOpacity style={styles.btnClear} onPress={() => {
                                    onChangeTextSearch('');
                                    onHandleSearch();
                                    setVisibleSearch(false);
                                }}>
                                    <Image
                                        style={styles.iconClose}
                                        source={ICONS.close} />
                                </TouchableOpacity>

                            </View>
                            <TouchableOpacity onPress={() => onHandleSearch()}>
                                <Text style={styles.searchText}>Search</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    {isVisibleSearch === false &&
                        <View style={styles.icons}>
                            <TouchableOpacity onPress={() => setVisibleSearch(true)}>
                                <Image
                                    style={styles.icon}
                                    source={ICONS.search}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('Camera')}>
                                <Image
                                    style={styles.icon}
                                    source={ICONS.camera}
                                />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            </View>
            <SafeAreaView style={styles.body}>
                <FlatList
                    columnWrapperStyle={{ justifyContent: 'space-between' }}
                    contentContainerStyle={styles.videoList}
                    numColumns={4}
                    data={videoList}
                    extraData={videoSelected}
                    keyExtractor={(item: VideoModel, index: number) => index.toString()}
                    onEndReached={() => handleLoadMore()}
                    onEndReachedThreshold={0}
                    renderItem={({ item, index }) => (

                        <TouchableOpacity style={styles.videoItem}
                            onLongPress={() => onSelectedItem(item.url, index)}
                            onPress={() => navigation.navigate("VideoPlay", { video: item })}>
                            <Image
                                style={styles.video}
                                source={{ uri: item.url }}
                            />
                            {
                                (videoSelected.includes(index)) &&
                                <Image
                                    style={styles.iconCheck}
                                    source={ICONS.check}
                                />
                            }
                            {
                                !(videoSelected.includes(index)) &&
                                <Text style={styles.duration}>{item.duration}</Text>

                            }

                        </TouchableOpacity>
                    )}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }

                />
            </SafeAreaView>


        </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    preview: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
    },
    body: {
        paddingLeft: 10,
        paddingRight: 10,
        height: Dimensions.get('window').height - 80,
        backgroundColor: COLORS.white,
    },
    videoList: {
        flexDirection: 'column',
        paddingTop: 5,
    },
    video: {
        width: '100%',
        height: '100%',
        borderRadius: 5,
        resizeMode: 'cover'
    },
    videoItem: {
        marginBottom: 5,
        width: Dimensions.get('window').width / 5,
        height: 90,
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconPlay: {
        position: 'absolute',
        width: 20,
        height: 20,
        tintColor: COLORS.white,
    },
    iconCheck: {
        position: 'absolute',
        width: 10,
        height: 10,
        bottom: 10,
        right: 10
    },
    duration: {
        position: 'absolute',
        bottom: 5,
        right: 5,
        fontSize: 8,
        color: COLORS.white,
    },
    containerHeader: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: COLORS.gray,
        borderBottomWidth: 0.8,
        padding: 8,
        justifyContent: 'space-between',
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    left: {
        width: '30%'
    },
    title: {
        color: COLORS.blue,
        fontSize: SIZES.h4
    },
    right: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    searchText: {
        color: COLORS.blue
    },
    icons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.darkgray,
        marginLeft: 15
    },
    iconBackButton: {
        width: '8%'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue
    },
    searchBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '75%',
        borderRadius: 10,
    },
    textSearch: {
        position: 'relative',
        color: 'black',
        width: '100%',
        padding: 2,
        paddingLeft: 25,
        borderWidth: 0,
        backgroundColor: COLORS.gray
    },
    iconSearchInSearchBox: {
        width: 17,
        height: 17,
        position: 'absolute',
        marginLeft: 5,
        tintColor: COLORS.black
        // flex: 1,
    },
    btnClear: {
        width: 18,
        height: 18,
        borderRadius: 18,
        backgroundColor: COLORS.darkgray,
        position: 'absolute',
        right: 3,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconClose: {
        width: 10,
        height: 10,
        tintColor: COLORS.black,
    },
    loader: {
        marginTop: 50,
        alignItems: 'center',
        tintColor: 'red'
    }
});


export default Home;
