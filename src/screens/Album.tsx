import CameraRoll from '@react-native-community/cameraroll';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image, StyleSheet, Dimensions, FlatList, SafeAreaView, TextInput, RefreshControl, PermissionsAndroid } from 'react-native';
import { ICONS } from '../constants/icons';
import { constantStyles } from '../constants/styles';
import { COLORS, SIZES } from '../constants/theme';
import { AlbumModel } from '../models/AlbumModel';
const Album = () => {
    useEffect(() => {
        async function requestCameraPermission() {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                    {
                        title: "Media Permission",
                        message: "Access to storage",
                        buttonNegative: "Cancel",
                        buttonPositive: "OK"
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("You can use the camera");
                    getAlbums();
                } else {
                    console.log("Camera permission denied");
                }
            } catch (err) {
                console.warn(err);
            }
        }
        requestCameraPermission();
    }, [])
    const navigation = useNavigation<StackNavigationProp<any, any>>();
    const [refreshing, setRefreshing] = React.useState(false);
    const [albumList, setAlbumList] = useState<AlbumModel[]>([]);


    const onRefresh = useCallback(() => {
        setRefreshing(true);
        getAlbums();
        setRefreshing(false);
    }, []);

    const getAlbums = async () => {

        let albumList: AlbumModel[] = [];

        const albums: CameraRoll.Album[] = await CameraRoll.getAlbums({ assetType: 'Videos' });
        for (let album of albums) {
            const urlFirst = await getPhotosByAlbum(album.title);
            let albumModel: AlbumModel = new AlbumModel(album.title, album.count, urlFirst);
            albumList.push(albumModel);
        }
        setAlbumList(albumList);
    }


    const getPhotosByAlbum = async (albumName: string) => {
        const result = await CameraRoll.getPhotos({
            first: 1,
            groupName: albumName,
            assetType: 'Videos',
            include: ['fileSize', 'filename', 'imageSize', 'location']

        })
        return result.edges.length > 0 ? result.edges[0]?.node?.image.uri : '';

    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.containerHeader}>
                <View style={styles.left}>
                    <Text style={styles.title}>Albums</Text>
                </View>

                <View style={styles.right}>
                    <View style={styles.icons}>
                        <TouchableOpacity>
                            <Image
                                style={styles.icon}
                                source={ICONS.add}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
            <SafeAreaView style={styles.body}>
                <FlatList
                    columnWrapperStyle={{ justifyContent: 'space-between' }}
                    contentContainerStyle={styles.videoList}
                    numColumns={3}
                    data={albumList}
                    keyExtractor={(item: AlbumModel) => item.title}
                    renderItem={({ item }) => (

                        <TouchableOpacity style={styles.album} onPress={() => navigation.navigate("VideoAlbum", { album: item.title })}>
                            <Image
                                style={styles.albumImg}
                                source={{ uri: item.firstImgUrl }}
                            />
                            <Text style={styles.alBumInfo}> {item.title} | {item.count}</Text>
                        </TouchableOpacity>
                    )}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }

                />
            </SafeAreaView>

        </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
    },
    body: {
        paddingLeft: 10,
        paddingRight: 10,
        height: Dimensions.get('window').height - 80,
        backgroundColor: COLORS.white,
    },
    videoList: {
        flexDirection: 'column',
        marginTop: 5
    },
    album: {
        marginBottom: 10,
        width: Dimensions.get('window').width / 3.5,
        height: 120,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    albumImg: {
        width: '100%',
        height: '90%',
        borderRadius: 5,
        resizeMode: 'cover',
    },

    alBumInfo: {
        color: COLORS.black,
        fontSize: 10
    },
    containerHeader: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: COLORS.gray,
        borderBottomWidth: 0.8,
        padding: 8,
        justifyContent: 'space-between',
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    left: {
        width: '30%'
    },
    title: {
        color: COLORS.blue,
        fontSize: SIZES.h4
    },
    right: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    searchText: {
        color: COLORS.blue
    },
    icons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.darkgray,
        marginLeft: 15
    },
    iconBackButton: {
        width: '8%'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue
    },
    searchBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 5,
        width: '75%',
        backgroundColor: COLORS.gray
    },
    textSearch: {
        position: 'relative',
        color: 'black',
        width: '100%',
        padding: 2,
        paddingLeft: 25,
        borderWidth: 0
    },
    iconSearchInSearchBox: {
        width: 17,
        height: 17,
        position: 'absolute',
        marginLeft: 5,
        tintColor: COLORS.black
        // flex: 1,
    },
    iconMicrophone: {
        width: 17,
        height: 17,
        position: 'absolute',
        // top: 0,
        right: 5,
    }

});

export default Album;