import CameraRoll from '@react-native-community/cameraroll';
import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import {
    Dimensions,
    FlatList,
    Image,
    RefreshControl,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { ICONS } from '../constants/icons';
import { constantStyles } from '../constants/styles';
import { COLORS, SIZES } from '../constants/theme';
import { VideoModel } from '../models/VideoModel';


const VideoAlbum = () => {

    const route = useRoute();
    const navigation = useNavigation();

    const albumName: string = route.params?.album;
    const [videoList, setVideoList] = useState<VideoModel[]>([]);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        getPhotosByAlbum(albumName);
    }, [albumName]);

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        getPhotosByAlbum(albumName);
        setRefreshing(false);
    }, []);

    const getPhotosByAlbum = async (albumName: string) => {
        console.log("🚀 ~ file: VideoAlbum.tsx ~ line 41 ~ getPhotosByAlbum ~ albumName", albumName)
        const result = await CameraRoll.getPhotos({
            first: 100,
            groupName: albumName,
            assetType: 'Videos',
            include: ['fileSize', 'filename', 'imageSize', 'location', 'playableDuration']

        })
        const list: CameraRoll.PhotoIdentifier[] = [];
        result.edges.forEach((item) => {
            list.push(item);
        });
        list.sort((a, b) => b.node.timestamp - a.node.timestamp);
        let listVideoModel: VideoModel[] = [];
        list.forEach((item: CameraRoll.PhotoIdentifier) => {
            const video: VideoModel = new VideoModel(item.node.image.uri, item.node.image.playableDuration !== null ? item.node.image.playableDuration : 0, item.node.timestamp);
            listVideoModel.push(video);
        });
        setVideoList(listVideoModel);
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
                    <Image
                        style={styles.iconBack}
                        source={ICONS.back} />
                    <Text style={styles.backText}>Back</Text>
                </TouchableOpacity>
                <View>
                    <Text style={{textAlign: 'left'}}> {albumName} </Text>
                </View>
                <TouchableOpacity>
                    {/* <Text style={styles.editText}>Edit</Text> */}
                </TouchableOpacity>
            </View>
            <SafeAreaView style={styles.body}>
                <FlatList
                    columnWrapperStyle={{ justifyContent: 'space-between' }}
                    contentContainerStyle={styles.videoList}
                    numColumns={4}
                    data={videoList}
                    keyExtractor={(item: VideoModel) => item.url}
                    renderItem={({ item }) => (

                        <TouchableOpacity style={styles.videoItem}
                            onLongPress={() => {
                                return (
                                    <Image
                                        style={styles.iconCheck}
                                        source={ICONS.check}
                                    />
                                );
                            }}
                            onPress={() => navigation.navigate('VideoPlay', { video: item })}>
                            <Image
                                style={styles.video}
                                source={{ uri: item.url }}
                            />
                            <Text style={styles.duration}>{item.duration}</Text>
                        </TouchableOpacity>
                    )}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }

                />
            </SafeAreaView>


        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
    },
    body: {
        paddingLeft: 10,
        paddingRight: 10,
        height: Dimensions.get('window').height - 80,
        backgroundColor: COLORS.white,
    },
    videoList: {
        flexDirection: 'column',
        marginTop: 5
    },
    video: {
        width: '100%',
        height: '100%',
        borderRadius: 5,
        resizeMode: 'cover'
    },
    videoItem: {
        marginBottom: 5,
        width: Dimensions.get('window').width / 5,
        height: 90,
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconPlay: {
        position: 'absolute',
        width: 20,
        height: 20,
        tintColor: COLORS.white,
    },
    iconCheck: {
        position: 'absolute',
        width: 20,
        height: 20,
        bottom: 10,
        right: 10
    },
    duration: {
        position: 'absolute',
        bottom: 5,
        right: 5,
        fontSize: 8,
        color: COLORS.white,
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: COLORS.gray,
        borderBottomWidth: 1,
        padding: 8,
        justifyContent: 'space-between',
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    back: {
        tintColor: COLORS.blue,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'nowrap'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue
    },
    backText: {
        color: COLORS.blue
    },
    editText: {
        color: COLORS.blue
    },
});

export default VideoAlbum;
