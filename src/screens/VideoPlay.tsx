import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useRef } from 'react';
import { useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Modal,
    Alert,
    Pressable
} from 'react-native';
import Video from 'react-native-video';
import Share from 'react-native-share';
import { ICONS } from '../constants/icons';
import { COLORS, SIZES } from '../constants/theme';
import { VideoModel } from '../models/VideoModel';
var RNFS = require('react-native-fs');
import Dialog from "react-native-dialog";
import { createTable, getDBConnection, saveTagVideo, deleteTagVideoByUrlVideo, deleteTable, getTags, getVideoByTagId } from '../services/db.services';
import { TagVideoModel } from '../models/TagVideoModel';
import { TagModel } from '../models/TagModel';
import DropDownPicker from 'react-native-dropdown-picker';



const VideoPlay = () => {

    const route = useRoute();
    const navigation = useNavigation();
    const video = useRef(null);
    const [mute, setMute] = useState(true);
    const [pause, setPause] = useState(false);
    const videoItem: VideoModel = route.params?.video;
    const url = videoItem.url;
    const [isShowConfirm, setIsShowConfirm] = useState(false);
    const [isShowInputTag, setIsShowInputTag] = useState(false);
    const [txtTags, onChangeText] = useState("");
    const [isShowPreviewVideo, setIsShowPreviewVideo] = useState(false);
    const [tags, setTags] = useState<TagModel[]>([]);
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState([]);
    const [items, setItems] = useState([{}]);

    const onPlayVideo = () => {
        setPause(!pause);
    }

    const onMute = () => {
        setMute(!mute);
    }

    const [result, setResult] = useState<string>('');


    const shareToTelegram = async () => {
        const shareOptions = {
            message: 'Share to your apps',
            url: url,
            title: 'Share'
        };

        try {
            const ShareResponse = await Share.open(shareOptions);
        } catch (error) {
            console.log('Error =>', error);
        }
    };

    const getDateVideo = () => {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let date = new Date(videoItem.timestamp * 1000);
        return (
            <View>
                <Text style={styles.date}>{date.getDate() + ' ' + monthNames[date.getMonth()]}</Text>
                <Text style={styles.time}>{date.getHours() + ':' + date.getMinutes()}</Text>
            </View>
        );
    }

    const onSeek = () => {
        console.log("🚀 ~ file: VideoPlay.tsx ~ line 78 ~ onSeek ~ onSeek", onSeek)
        // video.current.seek(1000);
    }
    const [modalVisible, setModalVisible] = useState(false);

    return (
        <SafeAreaView style={styles.container}>

            <TouchableOpacity style={[styles.body]} onPress={() => setIsShowPreviewVideo(!isShowPreviewVideo)}>
                <Video
                    style={[styles.video, , { backgroundColor: isShowPreviewVideo ? COLORS.black : COLORS.white }]}
                    source={{ uri: url }}
                    ref={video}
                    controls={false}
                    fullscreen={true}
                    playWhenInactive={false}
                    playInBackground={false}
                    resizeMode={'contain'}
                    pictureInPicture={true}
                    muted={mute}
                    paused={pause}
                    repeat={true}
                // onProgress = {()=> onSeek()}
                />
                {
                    pause &&
                    <Image
                        style={styles.videoPause}
                        source={ICONS.playButton} />
                }
            </TouchableOpacity>
            {!isShowPreviewVideo &&
                <View style={styles.header}>
                    <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
                        <Image
                            style={styles.iconBack}
                            source={ICONS.back} />
                        <Text style={styles.backText}>Back</Text>
                    </TouchableOpacity>
                    <View style={styles.dateTime}>
                        {getDateVideo()}
                    </View>
                    <TouchableOpacity style={styles.edit} onPress={async () => {
                        try {
                            const db = await getDBConnection();
                            const tags = await getTags(db);
                            let listTagItem: object[] = [];
                            for (let index = 0; index < tags.length; index++) {
                                const objItem = { label: tags[index].name, value: tags[index].id };
                                listTagItem.push(objItem);
                            }
                            setItems(listTagItem);
                            setTags(tags);
                        } catch (error) {
                            setTags([]);
                        }
                        setIsShowInputTag(true);
                    }}>
                        <Image
                            style={styles.iconTag}
                            source={ICONS.tag} />
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={isShowInputTag}
                            onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                setIsShowInputTag(!isShowInputTag);
                            }}
                        >
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <Text style={styles.modalText}>Hello World!</Text>
                                    <DropDownPicker
                                        multiple={true}
                                        min={0}
                                        max={5}
                                        open={open}
                                        value={value}
                                        items={items}
                                        setOpen={setOpen}
                                        setValue={setValue}
                                        setItems={setItems}
                                        style={styles.styleDropdown}
                                        maxHeight={100}
                                        zIndex={1000}
                                        zIndexInverse={1000}
                                        disableBorderRadius={true}

                                    />
                                    <View style={styles.footerModal}>
                                        <TouchableOpacity style={styles.btnCancel} onPress={() => setIsShowInputTag(false)}>
                                            <Text style={styles.txtCancel}>Cancel</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.btnOK} onPress={async () => {
                                            const db = await getDBConnection();
                                            // await deleteTable(db);
                                            await createTable(db);
                                            for (let index = 0; index < value.length; index++) {
                                                const tagsVideo: TagVideoModel = new TagVideoModel(url, parseInt(videoItem.durationNumber), videoItem.timestamp, value[index]);
                                                await saveTagVideo(db, tagsVideo)
                                            }
                                            setIsShowInputTag(false);
                                        }}>
                                            <Text style={styles.txtOK}>OK</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </TouchableOpacity>
                </View>
            }
            {!isShowPreviewVideo &&
                <View style={styles.footer}>
                    <TouchableOpacity onPress={shareToTelegram}>
                        <Image
                            style={styles.iconBottom}
                            source={ICONS.share} />
                    </TouchableOpacity>
                    <TouchableOpacity >
                        <Image
                            style={styles.iconBottom}
                            source={ICONS.heart} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onPlayVideo()}>
                        {
                            !pause &&
                            <Image
                                style={styles.iconBottom}
                                source={ICONS.pause} />
                        }
                        {
                            pause &&
                            <Image
                                style={styles.iconBottom}
                                source={ICONS.play} />
                        }
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onMute()}>
                        {
                            mute &&
                            <Image
                                style={styles.iconBottom}
                                source={ICONS.mute} />
                        }
                        {
                            !mute &&
                            <Image
                                style={styles.iconBottom}
                                source={ICONS.volume} />
                        }
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setIsShowConfirm(true)}>
                        <Image
                            style={styles.iconBottom}
                            source={ICONS.trash} />
                        <Dialog.Container visible={isShowConfirm}>
                            <Dialog.Title style={{ width: '70%' }}>Video delete</Dialog.Title>
                            <Dialog.Description>
                                Do you want to delete this video? You cannot undo this action.
                            </Dialog.Description>
                            <Dialog.Button label="Cancel" onPress={() => setIsShowConfirm(false)} />
                            <Dialog.Button label="Delete" onPress={() => {
                                RNFS.unlink(url)
                                    .then(async () => {
                                        setIsShowConfirm(false);
                                        console.log('FILE DELETED');
                                        const db = await getDBConnection();
                                        await deleteTagVideoByUrlVideo(db, url);
                                        navigation.goBack();
                                    })
                                    .catch((err: any) => {
                                        console.log(err);
                                    });
                            }} />
                        </Dialog.Container>
                    </TouchableOpacity>
                </View>
            }
        </SafeAreaView>

    );
}

var styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: COLORS.white
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: '#e4e4e4',
        borderBottomWidth: 1,
        padding: 8,
        justifyContent: 'space-between',
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
    },
    back: {
        tintColor: COLORS.blue,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        width: '30%',

    },
    dateTime: {
        justifyContent: 'center'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue
    },
    iconTag: {
        width: 20,
        height: 20,
        tintColor: COLORS.darkgray
    },
    backText: {
        color: COLORS.blue
    },
    edit: {
        alignItems: 'flex-end',
        width: '30%'
    },
    editText: {
        color: COLORS.blue
    },
    body: {
        position: 'relative',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',

    },
    video: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    footer: {
        position: 'absolute',
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomWidth: 1,
        padding: 8,
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
        borderTopWidth: 0.8,
        borderBottomColor: COLORS.gray,
        bottom: 0,
        left: 0
    },
    iconBottom: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue,
    },
    date: {
        fontSize: 12
    },
    time: {
        fontSize: 10,
        textAlign: 'center'
    },
    videoPause: {
        width: 50,
        height: 50,
        opacity: 1,
        tintColor: COLORS.white
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: COLORS.white,
        borderRadius: 5,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        height: '40%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 10,
        textAlign: 'left',
        color: '#686c70',
        fontWeight: '700'
    },
    styleDropdown: {
        borderWidth: 0.3,
        borderColor: COLORS.gray,
        zIndex: 11111111,
        flexShrink: 0,

        // flex:
    },
    footerModal: {
        marginTop: 10,
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flexGrow: 8

    },
    btnOK: {
        marginLeft: 10,
    },
    btnCancel: {
    },
    txtOK: {
        color: '#6dbdb5',
    },
    txtCancel: {
        color: '#6dbdb5'
    }
});

export default VideoPlay;
