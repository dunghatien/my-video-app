import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useCallback, useEffect } from 'react';
import { useState } from 'react';
import {
    Dimensions,
    Image,
    ListRenderItemInfo,
    RefreshControl,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View,
} from 'react-native';
import { RowMap, SwipeListView } from 'react-native-swipe-list-view';
import { DATA } from '../constants/data';
import { ICONS } from '../constants/icons';
import { COLORS, SIZES } from '../constants/theme';
import { getDBConnection, deleteTagVideo, createTable, saveTag, getTags, getImageTags, deleteTable } from '../services/db.services';
import Dialog from "react-native-dialog";
import { TagModel } from '../models/TagModel';


const Tags = () => {
    const navigation = useNavigation<StackNavigationProp<any, any>>();
    const [refreshing, setRefreshing] = React.useState(false);
    const [isVisibleSearch, setVisibleSearch] = useState(false);
    const [txtSearch, onChangeTextSearch] = useState("");
    const [txtTags, onChangeText] = useState("");
    const [isShowInputTag, setIsShowInputTag] = useState(false);
    const [tags, setTags] = useState<TagModel[]>([]);

    const loadDataCallback = useCallback(async () => {
        try {
            const db = await getDBConnection();
            let list: TagModel[] = [];
            const tags: TagModel[] = await getTags(db);
            for (let index = 0; index < tags.length; index++) {
                let result = await getImageTags(db, parseInt(tags[index].id));
                const tagTest: TagModel = new TagModel(tags[index].id, tags[index].name, tags[index].timestamp, result);
                list.push(tagTest);
            }
            setTags(list);
        } catch (error) {
            setTags([]);
        }
    }, []);


    useEffect(() => {
        loadDataCallback();
    }, [loadDataCallback]);

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        loadDataCallback();
        setRefreshing(false);
    }, []);

    const closeRow = (rowMap: RowMap<TagModel>, rowKey: string) => {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    };

    const deleteRow = async (rowMap: RowMap<TagModel>, rowKey: string) => {
        closeRow(rowMap, rowKey);
        const newData = [...tags];
        const prevIndex = tags.findIndex(item => item.id === rowKey);
        newData.splice(prevIndex, 1);
        setTags(newData);
        const db = await getDBConnection();
        deleteTagVideo(db, parseInt(rowKey));
    };


    const renderItem = (tags: ListRenderItemInfo<TagModel>) => {
        const date = new Date(tags.item.timestamp);
        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0');
        var yyyy = date.getFullYear();
        var hh = String(date.getHours()).padStart(2, '0');
        var minutes = String(date.getMinutes()).padStart(2, '0');
        var dateTime = `${dd}-${mm}-${yyyy} ${hh}:${minutes}`;
        return (
            <TouchableHighlight
                style={styles.rowFront}
                underlayColor={'#AAA'}
                onPress={() => {
                    navigation.navigate('TagVideo', { name: tags.item.name, id: tags.item.id });
                }}
            >
                <View style={styles.styleRow}>
                    {
                        tags.item.url !== '' &&
                        <Image
                            style={styles.imageVideo}
                            source={{ uri: tags.item.url }}
                        />
                    }
                    {
                        tags.item.url === '' &&
                        <Text style={{ width: 40, height: 40 }}></Text>
                    }

                    <View style={styles.txtTag}>
                        <Text style={styles.txtTagContent}>{tags.item.name}</Text>
                        <Text style={styles.dateTime}>{dateTime}</Text>
                    </View>
                    <Image
                        style={styles.threeDot}
                        source={ICONS.threeDot} />
                </View>
            </TouchableHighlight>
        );
    }

    const renderHiddenItem = (tags: ListRenderItemInfo<TagModel>, rowMap: RowMap<TagModel>) => {
        return (
            <View style={styles.rowBack}>
                <TouchableOpacity
                    style={[styles.backRightBtn, styles.backRightBtnRight]}
                    onPress={() => deleteRow(rowMap, tags.item.id)}
                >
                    <Text style={styles.backTextWhite}>Delete</Text>
                </TouchableOpacity>
            </View>
        );
    }

    const onHandleSearch = async () => {
        try {
            const db = await getDBConnection();
            const videoTags = await getTags(db, `where name like '%${txtSearch}%'`);
            setTags(videoTags);
        } catch (error) {
            setTags([]);
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.containerHeader}>
                {isVisibleSearch === false &&
                    <View style={styles.left}>
                        <Text style={styles.title}>{DATA.tags}</Text>
                    </View>
                }
                <View style={styles.right}>
                    {isVisibleSearch === true &&
                        <View style={styles.searchContainer}>
                            <TouchableOpacity style={styles.iconBackButton} onPress={() => setVisibleSearch(false)}>
                                <Image
                                    style={styles.iconBack}
                                    source={ICONS.back} />
                            </TouchableOpacity>
                            <View style={styles.searchBox}>
                                <TextInput
                                    style={styles.textSearch}
                                    placeholder="Search"
                                    keyboardType="default"
                                    autoFocus={true}
                                    onChangeText={onChangeTextSearch}
                                    value={txtSearch}
                                />
                                <Image
                                    style={styles.iconSearchInSearchBox}
                                    source={ICONS.search} />
                                <TouchableOpacity style={styles.btnClear} onPress={async () => {
                                    await onChangeTextSearch('');
                                    await loadDataCallback();
                                    setVisibleSearch(false);
                                }}>
                                    <Image
                                        style={styles.iconClose}
                                        source={ICONS.close} />
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={() => onHandleSearch()}>
                                <Text style={styles.searchText}>Search</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    {isVisibleSearch === false &&
                        <View style={styles.icons}>
                            <TouchableOpacity onPress={() => setVisibleSearch(true)}>
                                <Image
                                    style={styles.icon}
                                    source={ICONS.search}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => setIsShowInputTag(true)}>
                                <Image
                                    style={styles.icon}
                                    source={ICONS.add}
                                />
                                <Dialog.Container visible={isShowInputTag}>
                                    <Dialog.Title style={{ width: 100 }}>Enter name</Dialog.Title>
                                    <Dialog.Description>
                                        Please enter tag name for video
                                    </Dialog.Description>
                                    <Dialog.Input
                                        placeholder='Input tag'
                                        keyboardType='default'
                                        autoFocus={true}
                                        onChangeText={onChangeText}
                                        value={txtTags}
                                    ></Dialog.Input>
                                    <Dialog.Button label="Cancel" onPress={() => setIsShowInputTag(false)} />
                                    <Dialog.Button label="OK" onPress={async () => {
                                        const db = await getDBConnection();
                                        // await deleteTable(db);
                                        await createTable(db);
                                        const checkTagExit: TagModel[] = await getTags(db, `where name ='${txtTags}'`);
                                        if (checkTagExit.length === 0) {
                                            await saveTag(db, txtTags);
                                            await loadDataCallback();
                                        }

                                        setIsShowInputTag(false)
                                        onChangeText('');
                                    }} />
                                </Dialog.Container>
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            </View>
            <SafeAreaView

                style={styles.body}>
                <SwipeListView
                    data={tags}
                    renderItem={renderItem}
                    renderHiddenItem={renderHiddenItem}
                    rightOpenValue={-75}
                    previewRowKey={'0'}
                    previewOpenValue={-40}
                    previewOpenDelay={3000}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }
                />
            </SafeAreaView>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
    },
    body: {
        height: Dimensions.get('window').height - 80,
        backgroundColor: COLORS.white,
    },
    videoList: {
        flexDirection: 'column',
        marginTop: 5
    },
    album: {
        marginBottom: 10,
        width: Dimensions.get('window').width / 3.5,
        height: 120,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    albumImg: {
        width: '100%',
        height: '90%',
        borderRadius: 5,
        resizeMode: 'cover',
    },

    alBumInfo: {
        color: COLORS.black,
        fontSize: 10
    },
    containerHeader: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: COLORS.gray,
        borderBottomWidth: 0.8,
        padding: 8,
        justifyContent: 'space-between',
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    left: {
        width: '30%'
    },
    title: {
        color: COLORS.blue,
        fontSize: SIZES.h4
    },
    right: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    searchText: {
        color: COLORS.blue
    },
    icons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.darkgray,
        marginLeft: 15
    },
    iconBackButton: {
        width: '8%'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue
    },
    threeDot: {
        width: 15,
        height: 15,
        tintColor: COLORS.gray
    },
    searchBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        // borderRadius: 5,
        width: '75%',
        backgroundColor: COLORS.gray
    },
    textSearch: {
        position: 'relative',
        color: 'black',
        width: '100%',
        padding: 2,
        paddingLeft: 25,
        borderWidth: 0
    },
    iconSearchInSearchBox: {
        width: 17,
        height: 17,
        position: 'absolute',
        marginLeft: 5,
        tintColor: COLORS.black
        // flex: 1,
    },
    btnClear: {
        width: 18,
        height: 18,
        borderRadius: 18,
        backgroundColor: COLORS.darkgray,
        position: 'absolute',
        right: 3,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconClose: {
        width: 10,
        height: 10,
        tintColor: COLORS.black,
    },
    backTextWhite: {
        color: '#FFF',
    },
    imageVideo: {
        width: 40,
        height: 40
    },
    txtTag: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        marginLeft: 5,
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        height: '100%',
        paddingBottom: 5
    },
    txtTagContent: {
        fontSize: 12,
        color: COLORS.darkgray
    },
    dateTime: {
        fontSize: 8,
        color: COLORS.darkgray
    },
    rowFront: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: COLORS.gray,
        borderBottomWidth: 0.5,
        height: SIZES.height50,

    },
    styleRow: {
        display: 'flex',
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
    },
    rowBack: {
        alignItems: 'flex-end',
        backgroundColor: '#DDD',
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: SIZES.height50,
    },
    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0,
    },

});


export default Tags;
