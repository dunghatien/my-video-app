import React, { useEffect } from 'react';
import { StyleSheet, View, SafeAreaView, TouchableHighlight, Image, Text, TouchableOpacity } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { useCamera, UseCameraStateAction } from 'react-native-camera-hooks';
import { ICONS } from '../constants/icons';
import { COLORS, SIZES } from '../constants/theme';
import CameraRoll from '@react-native-community/cameraroll';
import { useState } from 'react';
import { useNavigation } from '@react-navigation/native';

const Camera = () => {
    useEffect(() => {
        getPhotosByAlbum();
    }, []);

    const [newestCamera, setNewestCamera] = useState('.mp4');
    const navigation = useNavigation();


    const getPhotosByAlbum = async () => {
        const result = await CameraRoll.getPhotos({
            first: 1,
            groupName: 'Camera',
            assetType: 'Videos',
            include: ['fileSize', 'filename', 'imageSize', 'location', 'playableDuration']

        })
        setNewestCamera(result.edges[0].node.image.uri);
    }

    const [{ cameraRef }, { recordVideo }] = useCamera(null);
    const [cameraType, setCameraType] = useState(RNCamera.Constants.Type.back);
    const [isRecording, setIsRecording] = useState(false);

    const recordVideoData = async () => {
        setIsRecording(true);
        try {
            const data = await recordVideo({ maxDuration: 3600 });
            const filePath = data.uri;
            CameraRoll.save(filePath, { 'type': 'video', 'album': 'Camera' })
                .then(res => {
                    setIsRecording(false);
                }).catch(err => {

                });

        } catch (error) {
        }

    }
    const [ss, setSS] = useState<number>(0);


    return (
        <SafeAreaView style={styles.container}>
            <RNCamera
                ref={cameraRef}
                type={cameraType}
                style={styles.camera}
                whiteBalance={RNCamera.Constants.WhiteBalance.sunny}
                playSoundOnCapture={true}
                defaultVideoQuality={RNCamera.Constants.VideoQuality['720p']}
                googleVisionBarcodeMode={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeMode.ALTERNATE}
                autoFocus={RNCamera.Constants.AutoFocus.on}
            >
            </RNCamera>
            <View style={styles.top}>
                <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
                    <Image
                        style={styles.iconBack}
                        source={ICONS.back} />
                </TouchableOpacity>
            </View>
            <View style={styles.bottom}>
                <TouchableOpacity>
                    {
                        newestCamera !== '' &&
                        <Image
                            style={styles.iconRecord}
                            source={{ uri: newestCamera }}
                        />
                    }

                </TouchableOpacity>
                <TouchableOpacity style={styles.btnVideoCamera} onPress={() => recordVideoData()}>
                    <Image
                        style={styles.iconRecord}
                        source={ICONS.videoCamera}
                    />

                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    if (cameraType === RNCamera.Constants.Type.back) {
                        setCameraType(RNCamera.Constants.Type.front);
                    } else {
                        setCameraType(RNCamera.Constants.Type.back)
                    }
                }}>
                    <Image
                        style={styles.iconRecord}
                        source={ICONS.switchCamera}
                    />
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );

}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    camera: {
        position: 'relative',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    top: {
        position: 'absolute',
        width: '100%',
        height: SIZES.height40,
        top: 0,
        left: 0,
        backgroundColor: 'rgba(161, 161, 156, 0.3)',
        opacity: 0.5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    back: {
        display: 'flex',
        alignItems: 'center'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.white
    },
    timeNotStartRecord: {
        color: COLORS.white,
        fontSize: 20
    },
    bottom: {
        position: 'absolute',
        width: '100%',
        height: SIZES.height50,
        bottom: 0,
        backgroundColor: 'rgba(161, 161, 156, 0.18)',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20

    },

    btnVideoCamera: {
        width: 40,
        height: 40,
        backgroundColor: COLORS.red,
        borderRadius: 30,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 1
    },
    iconRecord: {
        width: 25,
        height: 25,
        opacity: 1,
        tintColor: COLORS.white
    }
});
export default Camera;