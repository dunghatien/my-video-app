import React from 'react';
import {
    Dimensions,
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { ICONS } from '../constants/icons';
import { constantStyles } from '../constants/styles';
import { COLORS, SIZES } from '../constants/theme';


const Setting = () => {


    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.containerHeader}>
                <View style={styles.left}>
                    <Text style={styles.title}>Settings</Text>
                </View>

                <View style={styles.right}>
                    <View style={styles.icons}>
                        <TouchableOpacity>
                            <Image
                                style={styles.icon}
                                source={ICONS.threeDot}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
            <SafeAreaView style={styles.body}>

                <TouchableOpacity style={styles.settingItem}>
                    <View>
                        <Image
                            style={styles.iconSetting}
                            source={ICONS.wrench}
                        />
                    </View>
                    <View>
                        <Text style={styles.settingText}>General</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.settingItem}>
                    <View>
                        <Image
                            style={styles.iconSetting}
                            source={ICONS.play}
                        />
                    </View>
                    <View>
                        <Text style={styles.settingText}>View</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.settingItem}>
                    <View>
                        <Image
                            style={[styles.iconSetting, styles.iconSecurity]}
                            source={ICONS.fingerprint}
                        />
                    </View>
                    <View>
                        <Text style={styles.settingText}>Security</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.settingItem}>
                    <View>
                        <Image
                            style={styles.iconSetting}
                            source={ICONS.importData}
                        />
                    </View>
                    <View>
                        <Text style={styles.settingText}>Import data</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.settingItem}>
                    <View>
                        <Image
                            style={styles.iconSetting}
                            source={ICONS.exportData}
                        />
                    </View>
                    <View>
                        <Text style={styles.settingText}>Export data</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.settingItem}>
                    <View>
                        <Image
                            style={styles.iconSetting}
                            source={ICONS.dataCleaning}
                        />
                    </View>
                    <View>
                        <Text style={styles.settingText}>Cache</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.settingItem}>
                    <View>
                        <Image
                            style={styles.iconSetting}
                            source={ICONS.help}
                        />
                    </View>
                    <View>
                        <Text style={styles.settingText}>Help & Support</Text>
                    </View>
                </TouchableOpacity>
            </SafeAreaView>
        </SafeAreaView>

    );
}
var styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
    },
    body: {
        paddingLeft: 10,
        paddingRight: 10,
        height: Dimensions.get('window').height - 80,
        backgroundColor: COLORS.white,
    },
    settingItem: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 7,
        paddingBottom: 7,
        borderBottomWidth: 0.5,
        borderColor: COLORS.gray
    },
    iconSetting: {
        width: 15,
        height: 15,
        marginRight: 20,
    },
    settingText: {
        fontSize: 12,
        color:COLORS.darkgray
    },
    iconSecurity: {
    },
    containerHeader: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        height: SIZES.height40,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderBottomColor: COLORS.gray,
        borderBottomWidth: 0.8,
        padding: 8,
        justifyContent: 'space-between',
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    left: {
        width: '30%'
    },
    title: {
        color: COLORS.blue,
        fontSize: SIZES.h4
    },
    right: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    searchText: {
        color: COLORS.blue
    },
    icons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.gray,
        marginLeft: 15
    },
    iconBackButton: {
        width: '8%'
    },
    iconBack: {
        width: 20,
        height: 20,
        tintColor: COLORS.blue
    },
    searchBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 5,
        width: '75%',
        backgroundColor: COLORS.gray
    },
    textSearch: {
        position: 'relative',
        color: 'black',
        width: '100%',
        padding: 2,
        paddingLeft: 25,
        borderWidth: 0
    },
    iconSearchInSearchBox: {
        width: 17,
        height: 17,
        position: 'absolute',
        marginLeft: 5,
        tintColor: COLORS.black
        // flex: 1,
    },
    iconMicrophone: {
        width: 17,
        height: 17,
        position: 'absolute',
        // top: 0,
        right: 5,
    }
});
export default Setting;
