import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native'
import VideoPlay from './src/screens/VideoPlay';
import TabBar from './src/components/TabBar';
import Camera from './src/screens/Camera';
const Stack = createStackNavigator();

const App = () => {


  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName={'TabBar'}
      >
        <Stack.Screen name="TabBar" component={TabBar} />
        <Stack.Screen name="VideoPlay" component={VideoPlay} />
        <Stack.Screen name="Camera" component={Camera} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
